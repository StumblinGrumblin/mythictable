import Vue from 'vue'

class Asset {
    static loadAll(entities) {
        let results = [];
        for (let entity of Object.values(entities)) {
            let asset = entity.asset;
            if (!asset) continue;

            if (asset['@data']) {
                return Promise.resolve(entity);
            }

            Vue.set(asset, '@data', null);

            if (asset.kind === 'image' && asset.src) {
                let image = new Image();
                let promise = new Promise((resolve, reject) => {
                    image.addEventListener('load', () => {
                        asset['@data'] = image;
                        resolve(entity);
                    }, { once: true});
                    image.addEventListener('error', (ev) => {
                        // TODO: Better error handling
                        reject(ev);
                    }, { once: true});

                    image.src = asset.src;
                });

                results.push(promise);
            }
        }

        return results;
    }
}

export { Asset as default }
