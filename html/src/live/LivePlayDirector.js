// Use require() instead of import as SignalR does not use ES6-style export
const signalR = require("@aspnet/signalr");

class LivePlayDirector {
    constructor({ store }) {
        this.store = store;
        this.connection = null;
    }

    get sessionId () { return this.state.sessionId }
    set sessionId (id) { this.state.sessionId = id }

    get state () { return this.store.state.live }

    async init() {
        this.connection = new signalR.HubConnectionBuilder()
            .withUrl("/api/live")
            .build();

        this.connection.on('ConfirmDelta', this.onConfirmDelta.bind(this));
        this.connection.onclose((error) => {
            // FIXME: PoC only; needs to be mutation if used in prod
            this.store.state.live.connected = false;
        });
    }

    async connect() {
        this.state.connected = this.connection.start();
        try {
            await this.state.connected;
        }
        catch (e) {
            this.state.connected = false;
            throw e;
        }
        this.state.connected = true; // FIXME: PoC only; needs to be mutation if used in prod
        await this.initializeEntities();
    }

    async initializeEntities() {
        const resp = await fetch(`/api/s/${this.sessionId}/entities`);

        if (resp.status != 200) {
            return;
        }

        let entities = await resp.json();

        for (let entity of entities) {
            this.store.dispatch('entities/update', {
                id: entity.id,
                value: entity
            });
        }
    }

    onConfirmDelta(sessionDelta) {
        const entityDeltas = sessionDelta.entities;

        for (let delta of entityDeltas) {
            this.store.dispatch('entities/patch', delta);
        }
    }

    moveToken(id, newPos) {
        let patch = [
            { op: 'replace', path: '/token/pos', value: newPos}
        ];

        this.tryStep({ entities: [{id,  patch}] });
    }

    async tryStep(step) {
        await this.connection.invoke('submitDelta', step);
    }
}

export { LivePlayDirector as default }
